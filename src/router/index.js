import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/pages/Home'
import Testing from '@/pages/Testing'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/sembarang',
      name: 'something',
      component: Testing
    }
  ]
})
