const services = {
  MOVIE_POPULAR: 'movie/popular',
  MOVIE_TOP_RATED: 'movie/top_rated',
  MOVIE_LATEST: 'movie/latest',
  MOVIE_NOW_PLAYING: 'movie/now_playing',
  MOVIE_UPCOMING: 'movie/upcoming'
}

export default services
