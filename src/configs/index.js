import url from './url'
import services from './services'

export const URL = url
export const SERVICES = services
