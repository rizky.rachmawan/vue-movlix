'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // TMDB_KEY: '"5c3b5f822f7052ad5aaf92e4c4560618"',
  // BASE_URL: '"https://api.themoviedb.org/3/"'
})
